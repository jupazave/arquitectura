package internationalization.i18n;

import java.util.ListResourceBundle;
import java.util.Objects;

/**
 * Created by kevingamboa17 on 10/16/17.
 */
public class MyClassBundle_es extends ListResourceBundle {
    /**
     * Returns an array in which each item is a pair of objects in an
     * <code>Object</code> array. The first element of each pair is
     * the key, which must be a <code>String</code>, and the second
     * element is the value associated with that key.  See the class
     * description for details.
     *
     * @return an array of an <code>Object</code> array representing a
     * key-value pair.
     */
    @Override
    protected Object[][] getContents() {
        return contents;
    }

    private static Object[][] contents = {
            {"price"   , new Double(1.00) },
            { "currency", "MXN" }
    };
}
