package internationalization;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * Created by kevingamboa17 on 10/16/17.
 */
public class LocaleExample {


    public static void main(String[] args) {
        int option = getLocaleOption();
        Locale locale = getLocale(option);
        printMessage(locale);
    }


    private static Locale getLocale(int option) {
        switch (option) {
            case 1:
                return new Locale("en");
            case 2:
                return new Locale("es");
            case 3:
                new Locale("fr");
            default:
                return Locale.getDefault();
        }
    }


    private static int getLocaleOption(){
        System.out.println("En qué idioma deseas que te de un poquito de información?");
        System.out.println("Opción 1: Inglés");
        System.out.println("Opción 2: Español");
        System.out.println("Opción 3: Francés");
        System.out.println("Digíta el número de opción");
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        return option;

    }

    private static void printMessage(Locale locale) {
        ResourceBundle labels = ResourceBundle.getBundle("internationalization.i18n.MyBundle", locale);
        ResourceBundle bundle = ResourceBundle.getBundle("internationalization.i18n.MyClassBundle", locale);

        System.out.println(labels.getString("saludo"));

        System.out.print(bundle.getObject("price"));
        System.out.println(" " + bundle.getObject("currency"));

        System.out.println(labels.getString("despedida"));
    }
}
