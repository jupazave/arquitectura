package kwic_chain.commands;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

import java.util.Scanner;

/**
 * Created by kevingamboa17 on 10/11/17.
 */
public class NumberOfPhrasesScanner implements Command {
    public int howManyPhrases(){
        System.out.println("Cuántas frases introducirás?");
        return new Scanner(System.in).nextInt();
    }

    @Override
    public boolean execute(Context context) throws Exception {
        try {
            context.put("numberOfPhrases", howManyPhrases());
        } catch (Exception e) {
            return true;
        }
        return false;
    }
}
