package kwic_chain;

import org.apache.commons.chain.Command;
import org.apache.commons.chain.Context;

/**
 * Created by kevingamboa17 on 10/11/17.
 */
public class KwicChainTest {
    public static void main(String[] args) {
        Context context = new KwicContext();
        Command kwicGeneratorChain = new KwicGeneratorChain();

        try {
            kwicGeneratorChain.execute(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
