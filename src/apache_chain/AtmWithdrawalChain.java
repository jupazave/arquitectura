package apache_chain;

import apache_chain.Commands.FiftyDenominationDispenser;
import apache_chain.Commands.HundredDenominationDispenser;
import apache_chain.Commands.TenDenominationDispenser;
import org.apache.commons.chain.impl.ChainBase;

/**
 * Created by kevingamboa17 on 10/6/17.
 */
public class AtmWithdrawalChain extends ChainBase {

    public AtmWithdrawalChain() {
        super();
        addCommand(new HundredDenominationDispenser());
        addCommand(new FiftyDenominationDispenser());
        addCommand(new TenDenominationDispenser());
        addCommand(new AuditFilter());
    }
}
